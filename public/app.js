var LIST = '557903c9c0e8c6a2f25d48d5';
var MEMBERS = [
  { "id": "54043744e6a93252dc411004", "fullName": "Bruce SHI", "username": "shibocuhk" },
  { "id": "4f07ccc984eba61c0c0de489", "fullName": "Fei", "username": "qfei" },
  { "id": "557903d52097874faf5ac39b", "fullName": "ShiZhi", "username": "shizhi" },
  { "id": "50f68c4bcd1bde636b00562a", "fullName": "Wang Zuo", "username": "adjusted" },
  { "id": "52922641c73fa73920003e0b", "fullName": "hsluo", "username": "hsluo" },
];
var MEMBER_KEYS = {};
var REVIEWER_MAPPING = {
  "bruce": "Bruce SHI",
  "shibo": "Bruce SHI",
  "qfei": "Fei",
  "fei": "Fei",
  "shizhi": "ShiZhi",
  "chris": "ShiZhi",
  "zuo": "Wang Zuo",
  "hsluo": "hsluo",
  "haosi": "hsluo",
};
var storyPoints = {
  "Bruce SHI": 0,
  "Fei": 0,
  "ShiZhi": 0,
  "Wang Zuo": 0,
  "hsluo": 0,
};

var reviewPoints = _.clone(storyPoints);

var $table = $('#app-table');

MEMBERS.forEach(function (member) {
  $table.append('<tbody id="member-' + member.id + '"></tbody>');
  MEMBER_KEYS[member.id] = member.fullName;
});

Trello.authorize({
  name: 'Trello Planner',
  expiration: 'never',
  success: onAuthSuccess,
  error: function () { alert("Auth failed!"); }
});

var cards = [];

function onAuthSuccess() {
  Trello.get('/lists/' + LIST + '/cards?stickers=true&pluginData=true&fields=name,idMembers&actions=updateCard:idList', function (data) {
    data.forEach(function (card) {
      buildCard(card, LIST)
    });
    for (var k in storyPoints) {
      $('#summary').append([
        '<div class="member">',
        '<div class="name">', k, '</div>',
        '<div class="sp">SP: ', storyPoints[k], '</div>',
        '<div class="rp">RP: ', reviewPoints[k], '</div>',
        '</div>',
      ].join(''));
    }
  });
}

function buildCard(card) {
  card.idMembers.forEach(function (idMember) {
    var sp = 0, reviewer = '#', releasedAt = '', fr = '', pi = '';

    if (card.actions.length > 0 && card.actions[0].data.listAfter.id == LIST) {
      var releasedDate = moment(card.actions[0].date);
      if (releasedDate.isBefore(window.location.hash.substring(1) || '2017-01-01')) return;
      releasedAt = releasedDate.format('YYYY-MM-DD') || releasedAt;
    } else {
      return;
    }

    if (card.pluginData[0] && card.pluginData[0].idPlugin == '56d5e249a98895a9797bebb9') {
      var fields = JSON.parse(card.pluginData[0].value);
      sp = eval(fields['fields']["22"] || sp);
      reviewer = fields['fields']["20"] || reviewer;
    }

    if (reviewer) {
      reviewer = REVIEWER_MAPPING[reviewer.toLowerCase()] || ('#' + reviewer);
    }

    if (card.idMembers.length > 1) {
      sp = eval(sp) / card.idMembers.length;
    }

    var member = MEMBER_KEYS[idMember];
    if (!member) return;

    storyPoints[member] += sp;

    if (!reviewer.startsWith('#')) {
      reviewPoints[reviewer] += sp;
    }

    if (card.stickers.length > 0) {
      card.stickers.forEach(function (s) {
        if (s.image == 'frown') {
          fr += 'F';
          storyPoints[member] -= 0.5;
          if (!reviewer.startsWith('#')) storyPoints[reviewer] -= 0.25;
        }
        if (s.image == 'warning') {
          pi += 'P';
          storyPoints[member] -= 0.5 * sp;
          if (!reviewer.startsWith('#')) storyPoints[reviewer] -= 0.25 * sp;
        }
      });
    }

    $('#member-' + idMember).append([
      '<tr>',
      '<td>' + member + '</td>',
      '<td>' + card.name + '</td>',
      '<td>' + releasedAt + '</td>',
      '<td>' + reviewer + '</td>',
      '<td>' + fr + '</td>',
      '<td>' + pi + '</td>',
      '<td>' + sp + '</td>',
      '</tr>'
    ].join());
  });
}
